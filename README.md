# drf_react_docker_todo

---

## backend

-   Django
-   Django REST Framework

## frontend

-   React

## docker-compose.yml

```yml
version: "3"

services:
    api:
        build: ./backend
        command: python /app/manage.py runserver 0.0.0.0:8000
        environment:
            - ENVIRONMENT=development
            - SECRET_KEY=<SECRET_KEY>
            - DEBUG=1
        volumes:
            - ./backend:/app
        ports:
            - 8000:8000
        depends_on:
            - db
    db:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"
    client:
        build: ./frontend
        command: npm run start
        volumes:
            - /app/node_modules # container内のnode_modulesを使用
            - ./frontend:/app
        ports:
            - 3000:3000
        stdin_open: true

volumes:
    postgres_data:
```
