from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from .models import Post


User = get_user_model()


class PostsAPIModelTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        # create a user
        testuser1 = User.objects.create_user(
            username='testuser1',
            password='abcd1234',
        )
        testuser1.save()
        # create a post
        test_post = Post.objects.create(
            author=testuser1,
            title='post title',
            content='post content',
        )
        test_post.save()

    def test_blog_content(self):
        post = Post.objects.all()[0]
        author = f'{post.author}'
        title = f'{post.title}'
        content = f'{post.content}'
        self.assertEqual(author, 'testuser1')
        self.assertEqual(title, 'post title')
        self.assertEqual(content, 'post content')
