from rest_framework.generics import (
    ListCreateAPIView, RetrieveUpdateDestroyAPIView)

from .models import Post
from .serializers import PostSerializer
from .permissions import IsAuthorOfObject


class PostListAPIView(ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class PostRetrieveAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthorOfObject,)
