from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Post


class PostSerializer(ModelSerializer):
    # cover = SerializerMethodField()

    class Meta:
        model = Post
        fields = ('id', 'cover', 'author', 'title', 'content', 'timestamp',)

    # def get_cover(self, obj):
    #     if not obj.cover:
    #         return None
    #     request = self.context.get('request')
    #     image_uri = obj.cover.url
    #     return request.build_absolute_uri(image_uri)
