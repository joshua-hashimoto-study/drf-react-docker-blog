from django.urls import path

from .views import PostListAPIView, PostRetrieveAPIView

app_name = 'posts_api'

urlpatterns = [
    path('<uuid:pk>/', PostRetrieveAPIView.as_view(), name='post_detail'),
    path('', PostListAPIView.as_view(), name='post_list'),
]
