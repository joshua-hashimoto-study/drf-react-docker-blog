import uuid

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet
from django.urls import reverse


class PostQuerySet(models.QuerySet):
    def all(self) -> QuerySet:
        queryset = self.filter(is_active=True)
        return queryset


class PostManager(models.Manager):
    def get_queryset(self) -> PostQuerySet:
        return PostQuerySet(self.model, using=self._db)

    def all(self) -> QuerySet:
        return self.get_queryset().all()


class Post(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False)
    cover = models.ImageField(upload_to='covers/', blank=True)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    objects = PostManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("posts_api:post_detail", kwargs={"pk": self.pk})
