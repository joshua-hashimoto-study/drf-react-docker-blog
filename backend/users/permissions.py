from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsUsersProfile(BasePermission):

    def has_object_permission(self, request, view, obj):
        """
        read-only permissions are allowed for any request
        """
        # GET, OPTIONSなどの場合は一律で許可(閲覧許可)
        if request.method in SAFE_METHODS:
            return True
        # その他(POST, PUT, DELETE, etc)の場合は対象のオブジェクトのuserがログインユーザーと同じ場合のみ許可(編集許可)
        return obj == request.user
