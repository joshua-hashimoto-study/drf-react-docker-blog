from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import UserViewSet

app_name = 'users'

router = SimpleRouter()
router.register('', UserViewSet, basename='users')  # uuidでdetailにいく時も問題なく使える

urlpatterns = router.urls
