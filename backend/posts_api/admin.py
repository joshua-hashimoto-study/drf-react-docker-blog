from django.contrib import admin

from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'pk', 'timestamp', 'is_active',)


admin.site.register(Post, PostAdmin)
